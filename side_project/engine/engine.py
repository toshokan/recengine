import turicreate as tc

class Engine():
    def __init__(self, data_frame=None, extra_data=None, save_model=False, force_new=False):
        if self.data_frame:
            self.data = tc.load_sframe(data_frame).append(extra_data)
        else:
            self.data = self.extra_data

        self.death_hooks = []
        self._model_name = '{}_{}.model'.format(self.kind, self.name)

        if save_model:
            self.death_hooks.append(lambda: self._save())

        try:
            assert(not force_new)
            self.model = tc.load_model(self._model_name)
        except:
            self.train()

    def __del__(self):
        for f in self.death_hooks:
            try:
                f()
            except:
                # TODO logging
                pass

    def _predict(self, users, items):
        # Predict a rating of -1 for every item, for every user
        return tc.SFrame({'user':users,
                          'item_id':items,
                          'rating':[-1] * len(items)})

    def _recommend(self, users, number, **kwargs):
        return [-1] * number

    def predict(self, users, items):
        if isinstance(users, int):
            users = [users]
        elif isinstance(users, str):
            # TODO some kind of user:id lookup table, maybe
            pass

        if isinstance(items, int):
            items = [items]
        elif isinstance(items, str):
            # TODO see above
            pass

        # TODO test this
        if len(users) != len(items):
            _users = users * len(items)
            items = items * len(users)
            users = _users

        self._predict(self, users, items)

    def recommend(self, users, number, **kwargs):
        if isinstance(users, int):
            users = [users]
        elif isinstance(users, str):
            # TODO some kind of user:id lookup table, maybe
            pass

        self._recommend(users, number, **kwargs)

    def _train(self, **kwargs):
        pass

    def train(self, **kwargs):
        self._train(**kwargs)

    def _save(self):
        pass

    def save(self):
        self._save()
