import turicreate as tc

from . import Engine

class ExplicitFactorizationEngine(Engine):
    name = 'explicit_factorization_engine'

    def __init__(self, kind, data_frame=None, extra_data=tc.SFrame(), save_model=True, force_new=False):
        self.kind = kind

        super().__init__(data_frame=data_frame,
                         extra_data=extra_data,
                         save_model=save_model,
                         force_new=force_new)

    def _save(self):
        self.model.save(self._model_name)

    def _train(self, **kwargs):
        kwargs.setdefault('verbose', False) # TODO make configurable

        self.model = tc.recommender.ranking_factorization_recommender.create(self.data, 'user', 'item_id',
                                                                             target='score', **kwargs)
    def _predict(self, users, items):
        return self.model.predict(tc.SFrame({'user':users, 'item_id':items}))

    def _recommend(self, users, number, **kwargs):
        if users:
            recs = list(self.model.recommend(users=users, k=number, **kwargs)['item_id'])
            # TODO more advanced processing here
        else:
            recs = list(self.model.recommend(k=number, **kwargs)['item_id'])

        return recs
