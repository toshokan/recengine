from functools import reduce

# These imports appear unused, but we explicitly construct them from globals() later
from engine import ExplicitFactorizationEngine
from . import config

class RecEngine():
    def __init__(self, kind):
        self.engines = {}
        for engine in config.engines:
            self.engines[engine['name']] = globals()[engine['type']](kind, **engine['settings'])

    def _consent_top_n(self, user, number, stepsize=5, **kwargs):
        # Might be slow, we'll see
        # The idea here: first, this will add all candidates which the engines agree are in the top `stepsize`
        # then all the candidates which engines agree are in the top 2*`stepsize`; etc.
        scope = number
        consensus = {}
        recs = []

        for engine in config.consensus_engines:
            recs.append(self.engines[engine].recommend(user, number * 10, **kwargs))

        if len(recs) < 2:
            return recs[:number]

        while len(consensus) < number:
            scope += stepsize
            consensus = reduce(lambda s1, s2: set(s1[:scope]).intersection(set(s2[:scope])), recs)

        return list(consensus)

    def _consent_average(self, user, number, cutoff_factor=10, **kwargs):
        consensus = []
        recs = []
        avg_pos = {}

        for engine in config.consensus_engines:
            recs.append(self.engines[engine].recommend(user, number * 10, **kwargs))

        if len(recs) < 2:
            return recs[:number]

        for candidate in recs[0][:(number * cutoff_factor)]:
            avg_pos[candidate] = sum([l.index(candidate) for l in recs])

        for candidate, pos in sorted(avg_pos.iteritems(), key=lambda k,v: (v,k)):
            consensus.append(candidate)
            if len(consensus) >= number:
                break

        return consensus

    def _consent_hybrid(self, user, number, stepsize=10, pos_cutoff=0.3,
                        consent_cutoff=0.8, consensus_factor=2, **kwargs):
        scope = stepsize
        consensus = {}
        recs = []
        avg_pos = {}

        for engine in config.consensus_engines:
            recs.append(self.engines[engine].recommend(user, number * 10, **kwargs))

        if len(recs) < 2:
            return recs[:number]

        # Find (consensus_factor * number) candidates such that a consent_cutoff fraction of engines agrees
        # that they are within the top scope results, and that scope is minimal
        while len(consensus) < (consensus_factor * number) and scope < 10 * number:
            for candidate in recs[0][:scope]:
                if candidate in consensus:
                    continue

                num_consentors = len([None for x in recs if candidate in x[:scope]])
                if num_consentors < consent_cutoff * len(recs):
                    continue

                avg_pos[candidate] = sum([l.index(candidate) for l in recs])

            scope += stepsize

        # Then, find the top number results by average position
        for candidate, pos in sorted(avg_pos.iteritems(), key=lambda k,v: (v,k)):
            consensus.append(candidate)
            if len(consensus) >= number:
                break

        return consensus
