engines = [{'name':'explicit_factorization_engine',
            'type':'ExplicitFactorizationEngine',
            'settings':{
                'data_frame':None,
                'save_model':False,
                'force_new':False
            }}]

consensus_engines = ['explicit_factorization_engine']
