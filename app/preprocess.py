from collections import defaultdict
import csv

def process(item_cutoff, user_cutoff, kind='anime'):
    entries = []
    with open('{}_interactions.csv'.format(kind), 'r') as f:
        r = csv.reader(f)
        for row in r:
            if row[2] != '0':
                entries.append(row)

    user_count = defaultdict(int)
    item_count = defaultdict(int)

    for e in entries:
        item_count[e[1]] += 1

    valid_items = set([k for k,v in item_count.items() if v > item_cutoff])

    for e in entries:
        if e[1] in valid_items:
            user_count[e[0]] += 1

    valid_users = set([k for k,v in user_count.items() if v > user_cutoff])

    num_entries_orig = len(entries)
    num_users_orig = len(list(user_count.keys()))
    num_items_orig = len(list(item_count.keys()))
    num_users = len(valid_users)
    num_items = len(valid_items)

    entries = [e for e in entries if e[0] in valid_users and e[1] in valid_items]
    num_entries = len(entries)
    users_red = int(100-(num_users/(num_users_orig/100.0)))
    items_red = int(100-(num_items/(num_items_orig/100.0)))
    entries_red = int(100-(num_entries/(num_entries_orig/100.0)))

    print('Reduced number of users from {} to {}, yielding a reduction of: {}%'.format(num_users_orig,
                                                                                    num_users,
                                                                                    users_red))

    print('Reduced number of items from {} to {}, yielding a reduction of: {}%'.format(num_items_orig,
                                                                                    num_items,
                                                                                    items_red))

    print('Reduced number of entries from {} to {}, yielding a reduction of: {}%'.format(num_entries_orig,
                                                                                        num_entries,
                                                                                        entries_red))
    users_frac = 1-(users_red/100.0)
    items_frac = 1-(items_red/100.0)
    print('user,item matrix size reduction: {}%'.format(int(100 * (1-(users_frac*items_frac)))))
    return entries

def save(entries, kind='anime'):
    with open('{}_processed.csv'.format(kind), 'w') as f:
        w = csv.writer(f)
        w.writerow(['user', '_id', 'score'])
        for e in entries:
            w.writerow(e)

