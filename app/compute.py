import os
import logging
import json

import requests
import turicreate as tc
from google.cloud import storage

from api.clients import toshokan

def load_data(all_entries, type_):
    entries = [entry for entry in all_entries if entry["library_ref"].collection == type_]
    data_dict = {"_id": [], "user":[], "score":[]}
    for entry in entries:
        data_dict["_id"].append(entry["library_ref"].id)
        data_dict["user"].append(entry["user"])
        data_dict["score"].append(float(entry["score"]))
    return tc.SFrame(data_dict)

def load_item_data(type_):
    if type_ == 'anime':
        item_data = tc.SFrame({"_id": ["INVALID_ID"], "genres":[["None"]], "type": ["None"],
                               "episode_count": [0], "rating":["None"], "studios":[["None"]],
                               "start_date":[0], "source":["None"]})
        entries = list(toshokan[type_].find({}, {"_id": 1, "genres": 1, "type":1, "episode_count":1, "source":1, "rating":1, "studios":1, "start_date":1 }))
        for entry in entries:
            sd = [int(entry["start_date"].year)] if entry["start_date"] else [0]
            temp_frame = tc.SFrame({"_id": [entry["_id"]],
                                    "genres":[entry["genres"]],
                                    "type": [entry["type"]],
                                    "episode_count":[int(entry["episode_count"]) if entry["episode_count"] else -1],
                                    "source":[entry["source"]],
                                    "rating":[entry["rating"]],
                                    "studios":[entry["studios"]],
                                    "start_date": sd
            })


            item_data = item_data.append(temp_frame)

    elif type_ == 'manga':
        item_data = tc.SFrame({"_id": ["INVALID_ID"], "genres":[["None"]], "type": ["None"],
                               "chapter_count": [0], "serialization":["None"], "authors":[["None"]],
                               "start_date":[0]})

        entries = list(toshokan[type_].find({}, {"_id":1, "authors":1, "chapter_count":1, "start_date": 1, "genres": 1, "serialization": 1, "type":1}))
        for entry in entries:
            sd = [int(entry["start_date"].year)] if entry["start_date"] else [0]
            temp_frame = tc.SFrame({"_id": [entry["_id"]],
                                    "genres":[entry["genres"]],
                                    "authors": [[x.name for x in entry["authors"]]],
                                    "type": [entry["type"]],
                                    "chapter_count":[int(entry["chapter_count"]) if entry["chapter_count"] else -1],
                                    "serialization":[str(entry["serialization"])],
                                    "start_date": sd
            })

            item_data = item_data.append(temp_frame)

    return item_data

def download_interactions():
    # Connect to Google Cloud Storage, auth happens automagically, I have no idea how it works either.
    client = storage.Client()
    bucket = client.get_bucket('toshokan-recengine')
    for filename in ['manga_processed.csv', 'anime_processed.csv']:
        blob = bucket.get_blob(filename)
        blob.download_to_filename(filename)

# Retrieve list entries from MongoDB
entries = list(toshokan.lists.find({ 'score' : { "$exists": True, "$nin": [None, 0] } }, {"user":1, "score":1, "library_ref":1}))

anime_data = load_data(entries, "anime")
manga_data = load_data(entries, "manga")

# Add MAL data
download_interactions()
manga_data = tc.SFrame.read_csv('./manga_processed.csv', column_type_hints=[str, str, float]).append(manga_data)
anime_data = tc.SFrame.read_csv('./anime_processed.csv', column_type_hints=[str, str, float]).append(anime_data)

# Add item data
anime_item_data = load_item_data("anime")
manga_item_data = load_item_data("manga")

# MAL Anime data. This doesn't change so you don't really need to do anything with it. Takes about 15 seconds to load right now, so you might want to save it as an SFrame rather than a CSV.
#anime_data = tc.SFrame.read_csv('processed.csv')

# Anime lists of our users, in user, anime, score format. Must have a header, e.g. first line must be "anime","user","score". Just check the file if you don't understand what it should look like
#toshokan_anime_data = tc.SFrame.read_csv('result.csv')
#anime_data = anime_data.append()

# Same thing for manga data.
#manga_data = tc.SFrame.read_csv('manga_processed.csv')
# Not pictured here: getting the Toshokan user manga data and combining it.

# If you want to train an implicit model, do it here
#implicit_model = tc.recommender.ranking_factorization_recommender.create(anime_data, 'user', 'anime')

# Side data for anime, in an SFrame. Should have a column named "anime" that has the corresponding anime, and then however many other columns you want for side data. Ints, strings and lists of strings will definitely work (I've tested), I don't know about other data types.
# side_anime_data = tc.load_sframe('./side.sframe')
# Not pictured: side data for manga

# Filter out all anime without a score, so we can train an explicit model.
anime_data = anime_data[anime_data['score'] > 0]

# Train the actual model. Default settings, but with 60 max iterations (which I've found to give better results in testing).
explicit_anime_model = tc.recommender.ranking_factorization_recommender.create(anime_data, 'user', '_id', target='score', max_iterations=40, item_data=anime_item_data)

# Same for manga
manga_data = manga_data[manga_data['score'] > 0]
explicit_manga_model = tc.recommender.ranking_factorization_recommender.create(manga_data, 'user', '_id', target='score', max_iterations=40, item_data=manga_item_data, side_data_factorization=False)


# Save the models
#implicit_model.save('implicit.model')
explicit_anime_model.save('explicit_anime.model')
explicit_manga_model.save('explicit_manga.model')
